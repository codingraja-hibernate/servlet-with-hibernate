<%@ page isELIgnored="false" language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Servlet With Hibernate</title>

<style type="text/css">
	table {
		min-width: 700px; 
		border: 1px solid #CCC; 
	}
	table tr th { background: #000033; color: #FFF;}
	table tr td { border:1px solid #CCC;}
</style>
</head>
<body>
	<h1>CRUD Operations with Servlet and Hibernate</h1>
	<c:url value="/customer/register" var="registerUrl" />
	<form action="${registerUrl}" method="post">
		<table>
			<c:if test="${customer.id ne null}">
				<tr>
				<td>Customer ID:</td>
					<td><input type="text" name="id" value="${customer.id}" readonly="readonly"></td>
				</tr>
			</c:if>
			<tr>
				<td>First Name:</td>
				<td><input type="text" name="firstName" value="${customer.firstName}" required></td>
			</tr>
			<tr>
				<td>Last Name:</td>
				<td><input type="text" name="lastName" value="${customer.lastName}" required></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><input type="email" name="email" value="${customer.email}" required></td>
			</tr>
			<tr>
				<td>Mobile:</td>
				<td><input type="tel" pattern="[789][0-9]{9}" name="mobile" value="${customer.mobile}" required></td>
			</tr>

			<c:if test="${customer.id ne null}">
				<tr>
					<td><input type="submit" value="Update"></td>
				</tr>
			</c:if>
			<c:if test="${customer.id eq null}">
				<tr>
					<td><input type="submit" value="Save"></td>
				</tr>
			</c:if>
		</table>
	</form>
	<br>
	<h1>List of Customers</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>Mobile</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${customerList}" var="customer">
			<tr>
				<td>${customer.id}</td>
				<td>${customer.firstName}</td>
				<td>${customer.lastName}</td>
				<td>${customer.email}</td>
				<td>${customer.mobile}</td>
				
				<td><a href="<c:url value="/customer/update?"/>custId=${customer.id}">Update</a></td>
				<td><a href="<c:url value="/customer/delete?"/>custId=${customer.id}">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>